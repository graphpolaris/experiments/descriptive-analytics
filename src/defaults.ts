import {
  CategoryType,
  DefaultLayoutType,
  DefaultVisualPropertyType,
  VisualPropertyType,
} from "./types";

const defaultCategory: CategoryType = "nodelink";

const defaultLayout: Record<CategoryType, DefaultLayoutType> = {
  nodelink: "topology_driven",
  tabular: "adjacency_matrix",
  implicit: "leaves",
};

const defaultVisualProperty: Record<
  VisualPropertyType,
  DefaultVisualPropertyType
> = {
  color: "label",
  order: "degree",
  fill: "attribute",
};

export { defaultCategory, defaultLayout, defaultVisualProperty };
