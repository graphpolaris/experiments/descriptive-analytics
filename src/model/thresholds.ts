const stage1_thresholds = {
  size: 500,
  directed: 0.5,
  weighted: 0.5,
  density: 0.02,
  diameter: 3,
  modularity: 0.3,
  radius: 2,
  degreeAssortativity: 0,
  spatial: 0.5,
  components: 25,
  eigenvectorCentrality: 0.15,
  edgeAttributes: 3,
};

const stage2_thresholds = {
  nodelink: {
    density: 0.2,
    directed: 0.5,
    spatial: 0.5,
    temporal: 0.5,
    giniDegree: 0.2,
    communities: 3,
    nodeAttributes: 3,
    nodeTypes: 1,
    edgeAttributes: 3,
  },
  tabular: {
    volume: 300,
    weighted: 0.5,
    isMultiGraph: 0.5,
    nodeTypes: 1,
    edgeTypes: 1,
    edgeAttributes: 3,
  },
  implicit: {
    size: 1000,
    radius: 2,
    maxDegree: 4,
  },
};

const stage3_thresholds = {
  color: {
    nodeTypes: 3,
    communities: 3,
    modularity: 0.3,
    degreeAssortativity: 0,
  },
  order: {
    giniDegree: 0.2,
    nodeAttributes: 3,
    degreeRange: 4,
  },
  fill: {
    nodeAttributes: 3,
    degreeRange: 4,
  },
};

export { stage1_thresholds, stage2_thresholds, stage3_thresholds };
