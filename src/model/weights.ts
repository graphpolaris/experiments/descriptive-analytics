const stage1_weights = {
  size: {
    nodelink: -1,
    tabular: 1,
    implicit: -1,
  },
  weighted: {
    nodelink: 0,
    tabular: 1,
    implicit: 0,
  },
  directed: {
    nodelink: 1,
    tabular: 0,
    implicit: -1,
  },
  diameter: {
    nodelink: 1,
    tabular: -1,
    implicit: 0,
  },
  density: {
    nodelink: -1,
    tabular: 1,
    implicit: 0,
  },
  radius: {
    nodelink: 1,
    tabular: -1,
    implicit: 0,
  },
  modularity: {
    nodelink: -1,
    tabular: 1,
    implicit: 0,
  },
  degreeAssortativity: {
    nodelink: 0,
    tabular: 1,
    implicit: 1,
  },
  spatial: {
    nodelink: 1,
    tabular: 0,
    implicit: -1,
  },
  components: {
    nodelink: 0,
    tabular: 0,
    implicit: 1,
  },
  eigenvectorCentrality: {
    nodelink: -1,
    tabular: 1,
    implicit: 0,
  },
  edgeAttributes: {
    nodelink: 0,
    tabular: 0,
    implicit: -1,
  },
};

const stage2_weights = {
  nodelink: {
    density: {
      topology_driven: 1,
      attribute_driven_faceting: 0,
      attribute_driven_positioning: 0,
    },
    directed: {
      topology_driven: -1,
      attribute_driven_faceting: 0,
      attribute_driven_positioning: 1,
    },
    spatial: {
      topology_driven: -1,
      attribute_driven_faceting: -1,
      attribute_driven_positioning: 1,
    },
    temporal: {
      topology_driven: -1,
      attribute_driven_faceting: -1,
      attribute_driven_positioning: 1,
    },
    giniDegree: {
      topology_driven: 1,
      attribute_driven_faceting: 0,
      attribute_driven_positioning: 0,
    },
    communities: {
      topology_driven: 1,
      attribute_driven_faceting: 0,
      attribute_driven_positioning: 1,
    },
    nodeAttributes: {
      topology_driven: 0,
      attribute_driven_faceting: 1,
      attribute_driven_positioning: 1,
    },
    nodeTypes: {
      topology_driven: 0,
      attribute_driven_faceting: 0,
      attribute_driven_positioning: 1,
    },
    edgeAttributes: {
      topology_driven: 0,
      attribute_driven_faceting: 1,
      attribute_driven_positioning: 0,
    },
  },
  tabular: {
    volume: {
      adjacency_matrix: 1,
      quilts: -1,
      biofabric: -1,
    },
    weighted: {
      adjacency_matrix: 1,
      quilts: 0,
      biofabric: 0,
    },
    isMultiGraph: {
      adjacency_matrix: 1,
      quilts: 0,
      biofabric: 0,
    },
    nodeTypes: {
      adjacency_matrix: 0,
      quilts: 1,
      biofabric: 1,
    },
    edgeTypes: {
      adjacency_matrix: 0,
      quilts: 0,
      biofabric: 1,
    },
    edgeAttributes: {
      adjacency_matrix: -1,
      quilts: 0,
      biofabric: 0,
    },
  },
  implicit: {
    size: {
      leaves: 1,
      inner_nodes_leaves: 0,
    },
    radius: {
      leaves: 0,
      inner_nodes_leaves: 1,
    },
    maxDegree: {
      leaves: 0,
      inner_nodes_leaves: -1,
    },
  },
};

const stage3_weights = {
  color: {
    nodeTypes: {
      label: -1,
      attribute: 0,
      cluster: 1,
    },
    communities: {
      label: 1,
      attribute: 0,
      cluster: -1,
    },
    modularity: {
      label: 0,
      attribute: 0,
      cluster: 1,
    },
    degreeAssortativity: {
      label: 1,
      attribute: 1,
      cluster: 0,
    },
  },
  order: {
    giniDegree: {
      degree: 1,
      attribute: 0,
    },
    nodeAttributes: {
      degree: 0,
      attribute: 1,
    },
    degreeRange: {
      degree: 1,
      attribute: 0,
    },
  },
  fill: {
    nodeAttributes: {
      attribute: 1,
      edge: 0,
    },
    degreeRange: {
      attribute: 0,
      edge: 1,
    },
  },
};

export { stage1_weights, stage2_weights, stage3_weights };
