import Graphology from "graphology";
import { bidirectional } from "graphology-shortest-path/unweighted";

/**
 * Calculates different path measures for the input graph
 * @param {Graphology} graph The graph
 * @return {Object} Path measures
 */
export const calculatePath = (graph: Graphology) => {
  try {
    let totalPathLength = 0;
    let pathCount = 0;
    let minShortestPath = Infinity;
    let maxShortestPath = -Infinity;

    graph.forEachNode((node1) => {
      graph.forEachNode((node2) => {
        if (node1 !== node2) {
          const path = bidirectional(graph, node1, node2);
          if (path !== null) {
            totalPathLength += path.length;
            pathCount++;
            if (path.length < minShortestPath) {
              minShortestPath = path.length;
            }
            if (path.length > maxShortestPath) {
              maxShortestPath = path.length;
            }
          }
        }
      });
    });

    const avgShortestPath = totalPathLength / pathCount;

    const radius = minShortestPath;
    const diameter = maxShortestPath;

    return {
      avgShortestPath,
      radius,
      diameter,
    };
  } catch (error) {
    console.error("Error calculating path measures:", error);
    return {
      avgShortestPath: 0,
      radius: 0,
      diameter: 0,
    };
  }
};
