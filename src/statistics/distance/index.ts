import Graphology from "graphology";
import { calculatePath } from "./path";

/**
 * Calculates all connection measures for the graph
 * @param {Graphology} graphology graph
 * @return {number} Assortativity
 */
export const distance = (graph: Graphology) => {
  const { avgShortestPath, radius, diameter } = calculatePath(graph);

  return {
    avgShortestPath,
    radius,
    diameter,
  };
};

export * from "./path";
