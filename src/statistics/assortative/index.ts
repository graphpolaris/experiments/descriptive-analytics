import Graphology from "graphology";
import { calculateAssortativity } from "./assortativity";

export const assortative = (graph: Graphology) => {
  const degreeAssortativity = calculateAssortativity(graph);

  return { degreeAssortativity };
};
