import Graphology from "graphology";

/**
 * Calculates graph degree assortativity
 * @param {Graphology} graphology graph
 * @return {number} Degree assortativity
 */
export const calculateAssortativity = (graph: Graphology): number => {
  try {
    let edgeCount = 0;
    let sum_jk = 0;
    let sum_j_plus_k = 0;
    let sum_j_squared_plus_k_squared = 0;

    graph.forEachEdge((edgeId) => {
      const [fromNode, toNode] = graph.extremities(edgeId);
      const degreeFrom = graph.degree(fromNode);
      const degreeTo = graph.degree(toNode);

      edgeCount++;

      if (graph.type === "directed") {
        // For directed graphs, count each edge once
        sum_jk += degreeFrom * degreeTo;
        sum_j_plus_k += degreeFrom + degreeTo;
        sum_j_squared_plus_k_squared += degreeFrom ** 2 + degreeTo ** 2;
      } else {
        // For undirected graphs, count each edge twice
        sum_jk += degreeFrom * degreeTo * 2;
        sum_j_plus_k += (degreeFrom + degreeTo) * 2;
        sum_j_squared_plus_k_squared += (degreeFrom ** 2 + degreeTo ** 2) * 2;
      }
    });

    const numerator = edgeCount * sum_jk - sum_j_plus_k ** 2 / 4;
    const denominator =
      edgeCount * sum_j_squared_plus_k_squared - sum_j_plus_k ** 2 / 4;

    return numerator / denominator;
  } catch (error) {
    console.error("Error calculating assortativity:", error);
    return 0;
  }
};
