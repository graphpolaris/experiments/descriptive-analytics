import Graphology from "graphology";
import { GraphDirections, Statistics } from "../types";
import { distance } from "./distance";
import { assortative } from "./assortative";
import { partitioning } from "./partitioning";
import { cohesion } from "./cohesion";
import { characteristics } from "./graph";

const getStatistics = (
  graph: Graphology,
  directedness: GraphDirections,
  weighted: boolean,
  spatial: boolean,
  temporal: boolean,
  isMultiGraph: boolean,
  hasSelfLoop: boolean
): Statistics => {
  return {
    directed:
      directedness === "directed" ? 1 : directedness === "undirected" ? 0 : 0.5,
    weighted: weighted ? 1 : 0,
    spatial: spatial ? 1 : 0,
    temporal: temporal ? 1 : 0,
    isMultiGraph: isMultiGraph ? 1 : 0,
    hasSelfLoop: hasSelfLoop ? 1 : 0,
    ...characteristics(graph),
    ...cohesion(graph),
    ...partitioning(graph),
    ...distance(graph),
    ...assortative(graph),
  };
};

export { getStatistics };
