import Graphology from "graphology";
import louvain from "graphology-communities-louvain";

export const calculateCommunities = (graph: Graphology) => {
  const communities = new Set(Object.values(louvain(graph))).size;
  return communities;
};
