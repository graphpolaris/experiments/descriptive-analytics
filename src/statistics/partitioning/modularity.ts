import Graphology from "graphology";
import modularity from "graphology-metrics/graph/modularity";
import louvain from "graphology-communities-louvain";

/**
 * Calculates modularity of the graph
 * @param {Graphology} graphology graph
 * @return {number} Modularity of the graph
 */
export const calculateModularity = (graph: Graphology) => {
  try {
    if (graph.size < 0) return 0;

    louvain.assign(graph);

    const partition: { [key: string]: number } = {};
    graph.forEachNode((node) => {
      partition[node] = graph.getNodeAttribute(node, "community");
    });

    const mod = modularity(graph, partition);

    return mod;
  } catch (error) {
    console.error("Error calculating modularity:", error);
    return 0;
  }
};
