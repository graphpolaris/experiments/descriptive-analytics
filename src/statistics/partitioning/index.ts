import Graphology from "graphology";
import { calculateModularity } from "./modularity";
import { calculateCommunities } from "./communities";

export const partitioning = (graph: Graphology) => {
  const modularity = calculateModularity(graph);
  const communities = calculateCommunities(graph);
  return { modularity, communities };
};
