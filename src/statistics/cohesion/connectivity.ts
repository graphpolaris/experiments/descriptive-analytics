import Graphology from "graphology";
import {
  countConnectedComponents,
  largestConnectedComponent,
} from "graphology-components";

export const calculateConnectivity = (graph: Graphology) => {
  const components = countConnectedComponents(graph);
  const largest = largestConnectedComponent(graph);
  const largestComponentSize = largest.length;
  const inLargestComponent = (largestComponentSize / graph.order) * 100;

  return { components, inLargestComponent };
};
