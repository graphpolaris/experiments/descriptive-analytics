import Graphology from "graphology";
import { calculateDensity } from "./density";
import { calculateConnectivity } from "./connectivity";
import { calculateClustering } from "./clustering";

export const cohesion = (graph: Graphology) => {
  const density = calculateDensity(graph);
  const { components, inLargestComponent } = calculateConnectivity(graph);
  const clusteringCoefficient = calculateClustering(graph);

  return {
    density,
    components,
    inLargestComponent,
    clusteringCoefficient,
  };
};
