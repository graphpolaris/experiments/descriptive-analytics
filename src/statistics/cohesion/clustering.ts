import Graphology from "graphology";

/**
 * Calculates the global clustering coefficient of an undirected graph
 * @param {Graphology} graph The graph
 * @return {number} Clustering coefficient
 */
export const calculateClustering = (graph: Graphology) => {
  if (graph.order === 0) return 0;

  let totalTriangles = 0;
  let totalTriplets = 0;

  graph.forEachNode((node) => {
    const neighbors = graph.neighbors(node);
    const degree = neighbors.length;
    let localTriangles = 0;

    for (let i = 0; i < neighbors.length; i++) {
      const neighbor1 = neighbors[i];
      for (let j = i + 1; j < neighbors.length; j++) {
        const neighbor2 = neighbors[j];
        if (graph.hasEdge(neighbor1, neighbor2)) {
          localTriangles++;
        }
      }
    }

    totalTriangles += localTriangles;
    totalTriplets += (degree * (degree - 1)) / 2;
  });

  const clusteringCoefficient =
    totalTriplets === 0 ? 0 : totalTriangles / totalTriplets;

  return clusteringCoefficient;
};
