import Graphology from "graphology";

/**
 * Calculates density of the graph
 * Density is 0 for a null graph and 1 for a complete graph
 * Density of multi graphs can be higher than 1
 * Density of graphs with self loops can be higher than 1
 * @param {Graphology} graphology graph
 * @return {number} Density of graph
 */
export const calculateDensity = (graph: Graphology) => {
  const order = graph.order;
  const size = graph.size;

  if (order === 0) return 0; // Null graph

  let maxPossibleEdges;
  if (graph.type === "directed") {
    maxPossibleEdges = order * (order - 1);
  } else {
    maxPossibleEdges = (order * (order - 1)) / 2;
  }

  const densityValue = size / maxPossibleEdges;

  return densityValue;
};
