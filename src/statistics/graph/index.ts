import Graphology from "graphology";
import { calculateCentrality } from "./centrality";
import { calculateDegrees } from "./degree";
import { describe } from "./describe";

export const characteristics = (graph: Graphology) => {
  const { betweennessCentrality, closenessCentrality, eigenvectorCentrality } =
    calculateCentrality(graph);

  const { minDegree, maxDegree, averageDegree, giniDegree, degreeRange } =
    calculateDegrees(graph);

  const { size, volume, nodeTypes, edgeTypes, nodeAttributes, edgeAttributes } =
    describe(graph);

  return {
    size,
    volume,
    nodeTypes,
    edgeTypes,
    nodeAttributes,
    edgeAttributes,
    betweennessCentrality,
    closenessCentrality,
    eigenvectorCentrality,
    minDegree,
    maxDegree,
    averageDegree,
    degreeRange,
    giniDegree,
  };
};
