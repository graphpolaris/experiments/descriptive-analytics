import Graphology from "graphology";

/**
 * Extracts basic graph features
 * @param {Graphology} graphology graph
 * @return {number} Size
 * @return {number} Volume
 * @return {number} Number of node types
 * @return {number} Number of node attributes
 * @return {number} Number of edge types
 * @return {number} Number of node attributes
 */
export const describe = (graph: Graphology) => {
  let totalNodeAttributes = 0;

  const nodeLabels: Set<string> = new Set();
  graph.forEachNode((node, attributes) => {
    const attributeKeys = attributes ? Object.keys(attributes) : [];
    totalNodeAttributes += attributeKeys.length;
    if (attributes && attributes.labels) {
      attributes.labels.forEach((label: string) => nodeLabels.add(label));
    }
  });

  const averageNodeAttributes =
    graph.order > 0 ? totalNodeAttributes / graph.order : 0;

  let totalEdgeAttributes = 0;

  const edgeLabels: Set<string> = new Set();
  graph.forEachEdge((edge, attributes) => {
    const attributeKeys = attributes ? Object.keys(attributes) : [];
    totalEdgeAttributes += attributeKeys.length;
    if (attributes && attributes.labels) {
      attributes.labels.forEach((label: string) => edgeLabels.add(label));
    }
  });

  const averageEdgeAttributes =
    graph.size > 0 ? totalEdgeAttributes / graph.size : 0;

  return {
    size: graph.order,
    volume: graph.size,
    nodeTypes:
      Array.from(nodeLabels).length === 0 ? 1 : Array.from(nodeLabels).length,
    edgeTypes:
      Array.from(edgeLabels).length === 0 ? 1 : Array.from(edgeLabels).length,
    nodeAttributes: averageNodeAttributes,
    edgeAttributes: averageEdgeAttributes,
  };
};
