import Graphology from "graphology";
import closenessCentrality from "graphology-metrics/centrality/closeness";
import betweennessCentrality from "graphology-metrics/centrality/betweenness";
import eigenvectorCentrality from "graphology-metrics/centrality/eigenvector";
import { average } from "../../utils";

/**
 * Calculates different centrality measures for the input graph
 * @param {Graphology} graphology graph
 * @return {number} Betweenness centrality
 * @return {number} Closeness centrality
 * @return {number} Eigenvector centrality
 */
export const calculateCentrality = (graph: Graphology) => {
  const betweenness = average(betweennessCentrality(graph));
  const closeness = average(closenessCentrality(graph));
  const eigenvector = average(
    eigenvectorCentrality(graph, { maxIterations: 3000 })
  );

  return {
    betweennessCentrality: betweenness,
    closenessCentrality: closeness,
    eigenvectorCentrality: eigenvector,
  };
};
