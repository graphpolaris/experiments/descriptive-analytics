import Graphology from "graphology";

/**
 * Calculates graph degree measures
 * @param {Graphology} graphology graph
 * @return {number} Maximum degree of nodes
 * @return {number} Minimum degree of nodes
 * @return {number} Average degree of nodes
 * @return {number} Gini of degree distribution
 */
export const calculateDegrees = (
  graph: Graphology
): {
  maxDegree: number;
  minDegree: number;
  averageDegree: number;
  giniDegree: number;
  degreeRange: number;
} => {
  try {
    const nodeDegrees: { [key: string]: number } = {};

    graph.forEachNode((node) => {
      nodeDegrees[node] = graph.degree(node);
    });

    let totalDegree = 0;
    let maxDegree = 0;
    let minDegree = Infinity;

    for (const degree of Object.values(nodeDegrees)) {
      totalDegree += degree;
      if (degree > maxDegree) {
        maxDegree = degree;
      }
      if (degree < minDegree) {
        minDegree = degree;
      }
    }

    const averageDegree = graph.order > 0 ? totalDegree / graph.order : 0;

    const sortedDegrees = Object.values(nodeDegrees).sort((a, b) => a - b);

    let areaUnderCurve = 0;
    for (let i = 0; i < sortedDegrees.length; i++) {
      areaUnderCurve += (i + 1) * sortedDegrees[i];
    }

    const n = graph.order;
    const meanDegree = sortedDegrees.reduce((a, b) => a + b, 0) / n;

    let giniCoefficient = 0;
    if (meanDegree !== 0) {
      giniCoefficient = (n + 1 - 2 * (areaUnderCurve / (n * meanDegree))) / n;
    }

    const degreeRange = maxDegree - minDegree;

    return {
      maxDegree,
      averageDegree,
      minDegree: minDegree === Infinity ? 0 : minDegree,
      giniDegree: giniCoefficient,
      degreeRange,
    };
  } catch (error) {
    console.error("Error calculating degrees:", error);
    return {
      maxDegree: 0,
      averageDegree: 0,
      minDegree: 0,
      giniDegree: 0,
      degreeRange: 0,
    };
  }
};
