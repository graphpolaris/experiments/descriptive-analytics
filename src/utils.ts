import Graph from "graphology";
import { EdgeType, GraphDirections, GraphType } from "./types";

const parseGraph = (
  { nodes, edges }: GraphType,
  directedness: GraphDirections
) => {
  const { isMultiGraph, hasSelfLoop } = isMultiGraphOrSelfLoop(edges);
  const graph = new Graph({ type: directedness, multi: isMultiGraph });

  nodes.forEach((node) => {
    graph.addNode(node.id, node.attributes || {});
  });

  edges.forEach((edge) => {
    const attributes = {
      ...edge.attributes,
      ...(edge.weight !== undefined && { weight: edge.weight }),
    };
    graph.addEdge(edge.from, edge.to, attributes);
  });

  const weighted = isWeightedGraph(edges);

  return { graph, weighted, isMultiGraph, hasSelfLoop };
};

const isWeightedGraph = (edges: EdgeType[]): boolean => {
  return edges.some((edge) => edge.weight !== undefined);
};

const isMultiGraphOrSelfLoop = (
  edges: EdgeType[]
): { isMultiGraph: boolean; hasSelfLoop: boolean } => {
  const edgePairs = new Set<string>();
  let hasSelfLoop = false;

  for (const edge of edges) {
    if (edge.from === edge.to) {
      hasSelfLoop = true;
    }
    const pair = `${edge.from}-${edge.to}`;
    if (edgePairs.has(pair)) {
      return { isMultiGraph: true, hasSelfLoop };
    }
    edgePairs.add(pair);
  }

  return { isMultiGraph: false, hasSelfLoop };
};

const calculateScores = (
  statistics: Record<string, number>,
  weights: Record<string, any>,
  thresholds: Record<string, number>
) => {
  const scores: Record<string, number> = {};

  for (const metricKey in thresholds) {
    if (metricKey in statistics) {
      const metricValue = statistics[metricKey];
      const thresholdValue = thresholds[metricKey];

      if (metricValue > thresholdValue && weights[metricKey] !== undefined) {
        for (const visualizationType in weights[metricKey]) {
          if (!scores[visualizationType]) scores[visualizationType] = 0;
          scores[visualizationType] += weights[metricKey][visualizationType];
        }
      }
    }
  }

  return scores;
};

const average = (values: { [key: string]: number }) =>
  Object.values(values).reduce((a, b) => a + b, 0) /
  Object.values(values).length;

const roundToTwoDecimals = (num: number) => Math.round(num * 100) / 100;

export { parseGraph, average, roundToTwoDecimals, calculateScores };
