type GraphDirections = "directed" | "undirected" | "mixed";

type NodeType = {
  id: string;
  attributes: {
    [key: string]: any;
  };
};

type EdgeType = {
  id: string;
  from: string;
  to: string;
  weight?: number;
  attributes: {
    [key: string]: any;
  };
};

type GraphType = {
  nodes: NodeType[];
  edges: EdgeType[];
};

type Statistics = {
  size: number;
  volume: number;
  directed: number;
  weighted: number;
  spatial: number;
  temporal: number;
  hasSelfLoop: number;
  isMultiGraph: number;
  density: number;
  minDegree: number;
  maxDegree: number;
  averageDegree: number;
  degreeRange: number;
  giniDegree: number;
  diameter: number;
  avgShortestPath: number;
  radius: number;
  clusteringCoefficient: number;
  components: number;
  inLargestComponent: number;
  betweennessCentrality: number;
  closenessCentrality: number;
  eigenvectorCentrality: number;
  modularity: number;
  communities: number;
  degreeAssortativity: number;
  nodeTypes: number;
  nodeAttributes: number;
  edgeTypes: number;
  edgeAttributes: number;
};

type CategoryType = "nodelink" | "tabular" | "implicit";

type VisualPropertyType = "color" | "order" | "fill";

type DefaultLayoutType = "topology_driven" | "adjacency_matrix" | "leaves";

type DefaultVisualPropertyType = "label" | "degree" | "attribute";

export type {
  GraphDirections,
  GraphType,
  NodeType,
  EdgeType,
  Statistics,
  CategoryType,
  DefaultLayoutType,
  VisualPropertyType,
  DefaultVisualPropertyType,
};
