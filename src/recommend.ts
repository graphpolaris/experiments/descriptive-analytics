import {
  GraphType,
  CategoryType,
  DefaultLayoutType,
  VisualPropertyType,
  GraphDirections,
} from "./types";
import {
  stage1_weights,
  stage2_weights,
  stage3_weights,
} from "./model/weights";
import {
  stage1_thresholds,
  stage2_thresholds,
  stage3_thresholds,
} from "./model/thresholds";
import { defaultCategory, defaultLayout } from "./defaults";
import { getStatistics } from "./statistics";
import { parseGraph, calculateScores } from "./utils";

const categoryToPropertyMapping: Record<CategoryType, VisualPropertyType> = {
  nodelink: "color",
  tabular: "order",
  implicit: "fill",
};

const getRecommendation = (
  inputGraph: GraphType,
  directedness: GraphDirections = "undirected",
  spatial: boolean = false,
  temporal: boolean = false
) => {
  const { graph, weighted, isMultiGraph, hasSelfLoop } = parseGraph(
    inputGraph,
    directedness
  );

  const statistics = getStatistics(
    graph,
    directedness,
    weighted,
    spatial,
    temporal,
    isMultiGraph,
    hasSelfLoop
  );

  const stage1Scores = calculateScores(
    statistics,
    stage1_weights,
    stage1_thresholds
  );

  const category: CategoryType = Object.entries(stage1Scores)
    .map(([visualizationType, score]) => ({
      visualizationType: visualizationType as CategoryType,
      score,
    }))
    .reduce((best, current) => (current.score > best.score ? current : best), {
      visualizationType: defaultCategory,
      score: -Infinity,
    }).visualizationType;

  const stage2Scores = calculateScores(
    statistics,
    stage2_weights[category],
    stage2_thresholds[category]
  );

  const layout: DefaultLayoutType = Object.entries(stage2Scores)
    .map(([layoutType, score]) => ({
      layoutType: layoutType as DefaultLayoutType,
      score,
    }))
    .reduce((best, current) => (current.score > best.score ? current : best), {
      layoutType: defaultLayout[category],
      score: -Infinity,
    }).layoutType;

  const visualPropertyCategory: VisualPropertyType =
    categoryToPropertyMapping[category];

  const stage3Scores = calculateScores(
    statistics,
    stage3_weights[visualPropertyCategory],
    stage3_thresholds[visualPropertyCategory]
  );

  const visualProperty = Object.entries(stage3Scores)
    .map(([propertyType, score]) => ({
      propertyType: propertyType as VisualPropertyType,
      score,
    }))
    .reduce((best, current) => (current.score > best.score ? current : best), {
      propertyType: visualPropertyCategory,
      score: -Infinity,
    }).propertyType;

  return {
    category,
    layout,
    [visualPropertyCategory]: visualProperty,
    statistics,
  };
};

export { getRecommendation };
